package m9r

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestPathNode(t *testing.T) {
	tree := generatePathNodeTestData()

	t1, s1 := runHandler(tree, http.MethodGet, "/r2")
	test.That(t, t1, is.EqualTo(200))
	test.That(t, s1, has.Length(0))

	t2, s2 := runHandler(tree, http.MethodPost, "/r2")
	test.That(t, t2, is.EqualTo(0))
	test.That(t, s2, has.Length(0))

	t3, s3 := runHandler(tree, http.MethodGet, "/r2/r3")
	test.That(t, t3, is.EqualTo(201))
	test.That(t, s3, has.Length(0))

	t4, s4 := runHandler(tree, http.MethodPost, "/r2/r3")
	test.That(t, t4, is.EqualTo(202))
	test.That(t, s4, has.Length(0))

	t5, s5 := runHandler(tree, http.MethodPost, "/r4/r3")
	test.That(t, t5, is.EqualTo(203))
	test.That(t, s5, has.Length(1))
	test.That(t, s5[0], is.EqualTo("r4"))

	t6, s6 := runHandler(tree, http.MethodPut, "/r2/r3/r5")
	test.That(t, t6, is.EqualTo(204))
	test.That(t, s6, has.Length(1))
	test.That(t, s6[0], is.EqualTo("r5"))

	t7, s7 := runHandler(tree, http.MethodDelete, "/r2/r3")
	test.That(t, t7, is.EqualTo(0))
	test.That(t, s7, has.Length(0))
}

func generatePathNodeTestData() *pathNode {
	tree := newPathNode()

	tree.addHandlerForMethod(http.MethodGet, "/r2", handlerThatReturns(200))
	tree.addHandlerForMethod(http.MethodGet, "/r2/r3", handlerThatReturns(201))
	tree.addHandlerForMethod(http.MethodPost, "/r2/r3", handlerThatReturns(202))
	tree.addHandlerForMethod(http.MethodPost, "/*/r3", handlerThatReturns(203))
	tree.addHandlerForMethod(http.MethodPut, "/r2/r3/*", handlerThatReturns(204))
	tree.addHandlerForMethod(http.MethodPut, "/r2/r3/*/*", handlerThatReturns(205))

	return tree
}

func handlerThatReturns(status int) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(status)
	})
}

func runHandler(tree *pathNode, method string, path string) (int, []string) {
	methodSet, matchedWildSegments := tree.queryMethodSet(path)
	if methodSet == nil {
		return 0, matchedWildSegments
	}

	handler := methodSet.getHandlerForMethod(method)
	if handler == nil {
		return 0, matchedWildSegments
	}

	w := httptest.NewRecorder()
	r := httptest.NewRequest(method, "/", nil)
	handler.ServeHTTP(w, r)

	return w.Code, matchedWildSegments
}
