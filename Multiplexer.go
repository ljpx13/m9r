package m9r

import (
	"net/http"
	"sync"
)

// Multiplexer implements efficient dictionary lookups through path segments for
// routing HTTP requests.
type Multiplexer struct {
	tree *pathNode
	mx   *sync.RWMutex
}

const trailerNameForWildSegments = "m9r-wild-segments"

var _ http.Handler = &Multiplexer{}

// NewMultiplexer creates a new, empty multiplexer.
func NewMultiplexer() *Multiplexer {
	return &Multiplexer{
		tree: newPathNode(),
		mx:   &sync.RWMutex{},
	}
}

// Use adds a handler to the multiplexer for the provided method and path.
func (m *Multiplexer) Use(method, path string, handler http.Handler) {
	m.mx.Lock()
	defer m.mx.Unlock()
	m.tree.addHandlerForMethod(method, path, handler)
}

// ServeHTTP calls out to ExtendedServeHTTP and manages the scenario results
// automatically.
func (m *Multiplexer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	result := m.ExtendedServeHTTP(w, r)
	if result == nil {
		return
	}

	switch result.(type) {
	case *NotFoundScenarioResult:
		w.WriteHeader(http.StatusNotFound)
	case *MethodNotAllowedScenarioResult:
		w.WriteHeader(http.StatusMethodNotAllowed)
	case *PanicScenarioResult:
		w.WriteHeader(http.StatusInternalServerError)
	}
}

// ExtendedServeHTTP uses the registered handlers to provide a response to the
// incoming request.  It returns the details of any unexpected scenario
// encountered.  If the return result is nil, the request was handled
// successfully.
func (m *Multiplexer) ExtendedServeHTTP(w http.ResponseWriter, r *http.Request) (result ScenarioResult) {
	m.mx.RLock()
	defer m.mx.RUnlock()

	methodSet, matchedWildSegments := m.tree.queryMethodSet(r.URL.Path)
	if methodSet == nil {
		result = &NotFoundScenarioResult{
			RequestedPath: r.URL.Path,
		}
		return
	}

	handler := methodSet.getHandlerForMethod(r.Method)
	if handler == nil {
		result = &MethodNotAllowedScenarioResult{
			RequestedMethod: r.Method,
			AllowedMethods:  methodSet.allowedMethods(),
		}
		return
	}

	if r.Trailer == nil {
		r.Trailer = http.Header{}
	}

	r.Trailer[trailerNameForWildSegments] = matchedWildSegments

	defer func() {
		if p := recover(); p != nil {
			result = &PanicScenarioResult{
				PanicResult: p,
			}
		}
	}()

	handler.ServeHTTP(w, r)
	return
}
