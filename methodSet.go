package m9r

import "net/http"

// methodSet represents a set of handlers associated with a method.
type methodSet struct {
	mapping map[string]http.Handler
	methods []string
}

// newMethodSet returns a new, empty methodSet.
func newMethodSet() *methodSet {
	return &methodSet{
		mapping: map[string]http.Handler{},
		methods: []string{},
	}
}

// setHandlerForMethod sets the handler associated with an HTTP method.
func (m *methodSet) setHandlerForMethod(method string, handler http.Handler) {
	m.mapping[method] = handler

	m.methods = []string{}
	for method := range m.mapping {
		m.methods = append(m.methods, method)
	}
}

// getHandlerForMethod gets the handler for the provided method.  It returns nil
// if no method is found.
func (m *methodSet) getHandlerForMethod(method string) http.Handler {
	handler, _ := m.mapping[method]
	return handler
}

// allowedMethods returns the set of allowed methods for the methodSet.
func (m *methodSet) allowedMethods() []string {
	return m.methods
}
