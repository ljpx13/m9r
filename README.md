![](./icon.png)

# m9r

Package `m9r` implements a basic HTTP request multiplexer with support for
wildcard path segments.