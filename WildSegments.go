package m9r

import "net/http"

// WildSegments returns the ordered set of matched wild segments, if any.
func WildSegments(r *http.Request) []string {
	segments, _ := r.Trailer[trailerNameForWildSegments]
	return segments
}
