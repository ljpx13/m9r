package m9r

// ScenarioResult is an alias for types that are returned by the Multiplexer in
// unexpected scenarios e.g. NotFound, MethodNotAllowed, Panic etc.
type ScenarioResult interface{}

// NotFoundScenarioResult is returned when a matching route could not be found.
type NotFoundScenarioResult struct {
	RequestedPath string
}

// MethodNotAllowedScenarioResult is returned when a matching route is found but
// it doesn't accept the HTTP method used.
type MethodNotAllowedScenarioResult struct {
	RequestedMethod string
	AllowedMethods  []string
}

// PanicScenarioResult is returned when a panic is encountered when processing
// the request.
type PanicScenarioResult struct {
	PanicResult interface{}
}
