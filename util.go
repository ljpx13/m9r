package m9r

import (
	"path"
	"strings"
)

// normalizePath takes an input path and normalizes it:
//
// " \one\..\two\.\three\\four\ "
// "/two/three/four"
func normalizePath(v string) string {
	v = strings.TrimSpace(v)
	v = strings.ReplaceAll(v, `\`, "/")
	if !strings.HasPrefix(v, "/") {
		v = "/" + v
	}

	v = path.Clean(v)
	return v
}

// segmentPath splits the provided path into segments.  It expects the provided
// string to already have been normalized.  Behavior for a string that is not
// normalized is undefined.
//
// "/two/three/four"
// [ "two", "three", "four" ]
func segmentPath(v string) []string {
	spl := strings.Split(v, "/")
	return spl[1:]
}
