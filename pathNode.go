package m9r

import "net/http"

// pathNode represents a single path segment in a tree of nodes.  It is not
// thread-safe.
type pathNode struct {
	leaves    map[string]*pathNode
	wild      *pathNode
	methodSet *methodSet
}

// newPathNode creates a new, empty pathNode.
func newPathNode() *pathNode {
	return &pathNode{
		leaves: map[string]*pathNode{},
	}
}

// addHandlerForMethod adds or sets a handler for the provided method on the
// provided path.
func (p *pathNode) addHandlerForMethod(method, path string, handler http.Handler) {
	path = normalizePath(path)
	segments := segmentPath(path)
	p.addHandlerForMethodWithSegments(method, segments, handler)
}

// queryMethodSet queries for the methodSet associated with the provided path
// and method.
func (p *pathNode) queryMethodSet(path string) (*methodSet, []string) {
	path = normalizePath(path)
	segments := segmentPath(path)
	return p.queryMethodSetWithSegments(segments, nil)
}

// addHandlerForMethodWithSegments uses a set of segments directly to add to a
// node.
func (p *pathNode) addHandlerForMethodWithSegments(method string, segments []string, handler http.Handler) {
	if len(segments) < 1 {
		if p.methodSet == nil {
			p.methodSet = newMethodSet()
		}

		p.methodSet.setHandlerForMethod(method, handler)
		return
	}

	segment := segments[0]
	if segment == "*" {
		if p.wild == nil {
			p.wild = newPathNode()
		}

		p.wild.addHandlerForMethodWithSegments(method, segments[1:], handler)
		return
	}

	leaf, ok := p.leaves[segment]
	if !ok {
		leaf = newPathNode()
	}

	leaf.addHandlerForMethodWithSegments(method, segments[1:], handler)
	p.leaves[segment] = leaf
}

// queryMethodSetWithSegments queries for the methodSet with the provided
// segments.  It returns the methodSet, or nil, and the set of matched wild path
// segments.
func (p *pathNode) queryMethodSetWithSegments(segments []string, matchedWildSegments []string) (*methodSet, []string) {
	if len(segments) < 1 {
		return p.methodSet, matchedWildSegments
	}

	segment := segments[0]
	leaf, ok := p.leaves[segment]
	if ok {
		methodSet, matchedWildSegments := leaf.queryMethodSetWithSegments(segments[1:], matchedWildSegments)
		if methodSet != nil {
			return methodSet, matchedWildSegments
		}
	}

	if p.wild != nil {
		matchedWildSegments = append(matchedWildSegments, segment)
		return p.wild.queryMethodSetWithSegments(segments[1:], matchedWildSegments)
	}

	return nil, nil
}
