package m9r

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestMultiplexerDefaultNotFound(t *testing.T) {
	// Arrange.
	mplx := NewMultiplexer()
	svr := httptest.NewServer(mplx)
	defer svr.Close()

	// Act.
	resp, err := http.Get(fmt.Sprintf("%v/test", svr.URL))
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusNotFound))

	rawBody, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(rawBody), is.EqualTo(""))
}

func TestMultiplexerDefaultMethodNotAllowed(t *testing.T) {
	// Arrange.
	mplx := NewMultiplexer()
	mplx.Use(http.MethodPut, "/test", testHandler1)

	svr := httptest.NewServer(mplx)
	defer svr.Close()

	// Act.
	resp, err := http.Get(fmt.Sprintf("%v/test", svr.URL))
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusMethodNotAllowed))

	rawBody, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(rawBody), is.EqualTo(""))
}

func TestMultiplexerDefaultPanicScenario(t *testing.T) {
	// Arrange.
	mplx := NewMultiplexer()
	mplx.Use(http.MethodGet, "/panic", testHandler3)

	svr := httptest.NewServer(mplx)
	defer svr.Close()

	// Act.
	resp, err := http.Get(fmt.Sprintf("%v/panic", svr.URL))
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusInternalServerError))

	rawBody, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(rawBody), is.EqualTo(""))
}

func TestMultiplexerWildSegments(t *testing.T) {
	// Arrange.
	mplx := NewMultiplexer()
	mplx.Use(http.MethodGet, "/customer/*/order/*", testHandler1)

	svr := httptest.NewServer(mplx)
	defer svr.Close()

	// Act.
	resp, err := http.Get(fmt.Sprintf("%v/customer/123/order/456", svr.URL))
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusOK))

	rawBody, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(rawBody), is.EqualTo("123.456"))
}

func TestMultiplexerWildSegmentsReturnsEmptySliceForNoWildSegments(t *testing.T) {
	// Arrange.
	mplx := NewMultiplexer()
	mplx.Use(http.MethodGet, "", testHandler2)

	svr := httptest.NewServer(mplx)
	defer svr.Close()

	// Act.
	resp, err := http.Get(svr.URL)
	test.That(t, err, is.Nil())
	defer resp.Body.Close()

	// Assert.
	test.That(t, resp.StatusCode, is.EqualTo(http.StatusOK))

	rawBody, err := ioutil.ReadAll(resp.Body)
	test.That(t, err, is.Nil())
	test.That(t, string(rawBody), is.EqualTo("0"))
}

// -----------------------------------------------------------------------------

var testHandler1 = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	wildSegments := WildSegments(r)
	responseText := strings.Join(wildSegments, ".")

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%v", responseText)
})

var testHandler2 = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	wildSegments := WildSegments(r)
	responseText := fmt.Sprintf("%v", len(wildSegments))

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%v", responseText)
})

var testHandler3 = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	panic("there seems to be something wrong")
})
