package m9r

import (
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestUtilNormalizePath(t *testing.T) {
	testCases := []struct {
		g string
		e string
	}{
		{g: "", e: "/"},
		{g: " /  ", e: "/"},
		{g: `\`, e: "/"},
		{g: "./asd", e: "/asd"},
		{g: "../asd", e: "/asd"},
		{g: "/asd/", e: "/asd"},
		{g: ` \one\..\two\.\three\\four\ `, e: "/two/three/four"},
	}

	for _, testCase := range testCases {
		a := normalizePath(testCase.g)
		test.That(t, a, is.EqualTo(testCase.e))
	}
}

func TestUtilSegmentPath(t *testing.T) {
	// Arrange and Act.
	segments := segmentPath("/one/two/three")

	// Assert.
	test.That(t, segments, has.Length(3))
	test.That(t, segments[0], is.EqualTo("one"))
	test.That(t, segments[1], is.EqualTo("two"))
	test.That(t, segments[2], is.EqualTo("three"))
}
